import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  posts:Post[];
  constructor(private dataService:DataService) {
    
   }

  ngOnInit() {
    this.dataService.getPosts().subscribe((posts)=>{
      this.posts=posts.data;

    })
}

}
interface Post{
  id: number,
  name: string,
  sortname:string,
  phonecode: number
}
