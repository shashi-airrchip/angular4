import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

let apiURL = "http://localhost/shashikant/lunatexproject/lunatex/Api/";
// let apiURL = "http://13.58.191.39/lunatex/Api/";

@Injectable()
export class DataService {

  constructor(public http:Http) {

   }

   getPosts(){
     return this.http.get('http://13.58.191.39/laterzi/Api/all_country').map(res =>res.json());
   }


   httpPostData(credentials, type){
    debugger;
  	return new Promise((resolve, reject) =>{
  		
      let headers = new Headers(
      {
        'Content-Type' : 'application/x-www-form-urlencoded'
      });

      let options = new RequestOptions({ headers: headers });
  		debugger;
  		this.http.post(apiURL+type, JSON.stringify(credentials), options).subscribe(res =>{
        resolve(res.json());
  		}, (err) =>{
  			reject(err);
  		});
  	})
  }

  httpGetData(params, type){

    return new Promise((resolve, reject) =>{

      this.http.get(apiURL+type+params).subscribe(res =>{
        resolve(res.json());
      }, (err) =>{
        reject(err);
      });
    })
  }
}
