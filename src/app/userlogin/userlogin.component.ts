import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router} from '@angular/router';
import {DataService} from '../service/data.service';
import { Http } from '@angular/http';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {
 responseData : any;

  constructor(private router: Router, public dataservice: DataService, http:Http) { }

  ngOnInit() {
    
  }
  

  public onSubmit(user) :void{
    let userData = user;

    this.dataservice.httpPostData(userData, "insert_users").then((result) => {
      debugger;
      this.responseData = result;
      // console.log(this.responseData);

        if(this.responseData.status == "success") {
          alert(this.responseData.message);
          window.location.href="home";
        }
      }, (err) => {
        debugger;
        console.log(err);
    });
  }
  
  public onLogin(data) :void{
    alert("Entered Email id : " + data);
  }

}